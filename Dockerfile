# Use a Node.js base image
FROM node:20 AS build

# Set the working directory
WORKDIR /app

# Copy the project files
COPY . .

# Install dependencies
RUN npm install

# Build the Angular SSR project
RUN npm run build:ssr

# Use a lightweight Node.js image for the runtime
FROM node:20

# Set the working directory
WORKDIR /app

# Copy the built files from the previous stage
COPY --from=build /app/dist /app/dist

# Expose the port (adjust if your application runs on a different port)
EXPOSE 5000

# Start the server
CMD ["npm", "run", "serve:ssr:mysamplessrapp"]